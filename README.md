# legislaviz

Une visualisation graphique des résultats des élections législatives Françaises

# License

GNU Public License 3.0 (ou plus récent)

Le fond de carte est :
* Pour la France : sous license ODbl 1.0, trouvé ici : https://www.data.gouv.fr/fr/datasets/carte-des-circonscriptions-legislatives-2012-et-2017/,
  et légèrement modifié
* Pour le reste du monde : sous licence GPL 3.0 (ou plus récent), frabiqué à
  l'aide de https://geoman.io/geojson-editor
