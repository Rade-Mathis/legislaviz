/* -- SVG params -- */

const margin = {top: 20, right: 20, bottom: 20, left: 20};
const width = 760 - margin.left - margin.right;
const height = 650 - margin.top - margin.bottom;

const header_height = d3.select("header").node().offsetHeight;

const circle_r = 4;
const legend_to_bar_margin = 30;
const durations_time = 1000; // ms


/* -- Generate main components -- */
let svg = d3.select("div#viz-container").append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g") /* And a <g> within */
    .attr("id", "main-container")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

let tooltip = d3.select("body").append("div")
  .attr("class", "tooltip")
  .style("display", "none");
let tooltip_circo = tooltip.append("span");
let tooltip_1st_br = tooltip.append("br");
let tooltip_parti = tooltip.append("span");
tooltip.append("br");
let tooltip_votes = tooltip.append("span");


/* -- Global variables to hold files' contents -- */
let geojson;
let results;
let noms_propres;

/* -- Geographical stuff -- */

let projections = {
  "metropole": d3.geoOrthographic()
    .scale(3000)  // Zoom
    .translate([10 * width / 16, 3 * height / 8])  // On screen position
    .rotate([-2.2, -46.8])  // On earth position
    .clipAngle(90)  // Circular outer crop
    .precision(.1),
  "ile_de_france": d3.geoOrthographic()
    .scale(9000)  // x3
    .translate([5 * width / 16, 1 * height / 16])
    .rotate([-2.3, -48.9])
    .clipAngle(90)
    .precision(.1),
  "guadeloupe": d3.geoOrthographic()
    .scale(3000)
    .translate([width / 6, height / 2])
    .rotate([61.4, -16.2])
    .clipAngle(90)
    .precision(.1),
  "martinique": d3.geoOrthographic()
    .scale(3000)
    .translate([width / 6, 3 * height / 8])
    .rotate([61.0, -14.6])
    .clipAngle(90)
    .precision(.1),
  "guyane": d3.geoOrthographic()
    .scale(3000)
    .translate([1 * width / 16, 6 * height / 8])
    .rotate([53.5, -4.4])
    .clipAngle(90)
    .precision(.1),
  "reunion": d3.geoOrthographic()
    .scale(3000)
    .translate([1 * width / 16, 8 * height / 16])
    .rotate([-55.5, 21.1])
    .clipAngle(90)
    .precision(.1),
  "mayotte": d3.geoOrthographic()
    .scale(3000)
    .translate([1 * width / 16, 6 * height / 16])
    .rotate([-45.2, 12.8])
    .clipAngle(90)
    .precision(.1),
  "saint_m_et_b": d3.geoOrthographic()
    .scale(9000)  // 3x bigger
    .translate([width / 6, 2 * height / 8])
    .rotate([63.0, -18.0])
    .clipAngle(90)
    .precision(.1),
  "saint_p_et_m": d3.geoOrthographic()
    .scale(3000)
    .translate([width / 6, 1 * height / 8])
    .rotate([56.2, -47.0])
    .clipAngle(90)
    .precision(.1),
  "polynesie": d3.geoOrthographic()
    .scale(3000)
    .translate([width / 16, 4 * height / 16])
    .rotate([149.5, 17.6])
    .clipAngle(1)  // On ne garde que Tahiti et Moorea, désolé les autres :'(
    .precision(.1),
  "wallis_et_futuna": d3.geoOrthographic()
    .scale(9000)  // 3x bigger
    .translate([width / 16, 1 * height / 8]) //Dimensions
    .rotate([176.3, 13.3])
    .clipAngle(90)
    .precision(.1),
  "kanaky": d3.geoOrthographic()
    .scale(3000)
    .translate([7 * width / 16, 13.5 * height / 16])
    .rotate([-166.0, 21.2])
    .clipAngle(90)
    .precision(.1),
  "monde": d3.geoOrthographic()
    .scale(100)
    .translate([13 * width / 16, 14 * height / 16])
    .rotate([-6.3, -48.5])
    .clipAngle(90)
    .precision(.1)
};

function path(feature) {
  switch (feature.properties.nom_dpt) {
    case "GUADELOUPE":
      return d3.geoPath().projection(projections.guadeloupe)(feature);
    case "MARTINIQUE":
      return d3.geoPath().projection(projections.martinique)(feature);
    case "GUYANE":
      return d3.geoPath().projection(projections.guyane)(feature);
    case "LA REUNION":
      return d3.geoPath().projection(projections.reunion)(feature);
    case "MAYOTTE":
      return d3.geoPath().projection(projections.mayotte)(feature);
    case "SAINT-MARTIN/SAINT-BARTHELEMY":
      return d3.geoPath().projection(projections.saint_m_et_b)(feature);
    case "SAINT-PIERRE-ET-MIQUELON":
      return d3.geoPath().projection(projections.saint_p_et_m)(feature);
    case "POLYNESIE-FRANCAISE":
      return d3.geoPath().projection(projections.polynesie)(feature);
    case "WALLIS-ET-FUTUNA":
      return d3.geoPath().projection(projections.wallis_et_futuna)(feature);
    case "NOUVELLE-CALEDONIE":
      return d3.geoPath().projection(projections.kanaky)(feature);
    case "ETRANGER":
      return d3.geoPath().projection(projections.monde)(feature);
    case "PARIS":
    case "SEINE-SAINT-DENIS":
    case "HAUTS-DE-SEINE":
    case "VAL-DE-MARNE":
      return d3.geoPath().projection(projections.ile_de_france)(feature);
    default:
      return d3.geoPath().projection(projections.metropole)(feature);
  }
}

function get_feature_center(feature) {
  let nb = 0;
  let sum_x = 0;
  let sum_y = 0;
  for (let i of feature.geometry.coordinates) {
    for (let j of i) {
      if (feature.geometry.type === "Polygon") {
        ++nb;
        sum_x += j[0];
        sum_y += j[1];
      }
      else if (feature.geometry.type === "MultiPolygon") {
        for (let k of j) {
          if (feature.properties.nom_dpt === "POLYNESIE-FRANCAISE"
              && (k[0] < -150 || k[0] > -149 || k[1] < -18 || k[1] > -17)) {
            continue;  // Muting small polynesian islands, as clipped
          }
          ++nb;
          sum_x += k[0];
          sum_y += k[1];
        }
      }
    }
  }
  let mean_coords = [sum_x / nb, sum_y / nb];
  switch (feature.properties.nom_dpt) {
    case "GUADELOUPE":
      return projections.guadeloupe(mean_coords);
    case "MARTINIQUE":
      return projections.martinique(mean_coords);
    case "GUYANE":
      return projections.guyane(mean_coords);
    case "LA REUNION":
      return projections.reunion(mean_coords);
    case "MAYOTTE":
      return projections.mayotte(mean_coords);
    case "SAINT-MARTIN/SAINT-BARTHELEMY":
      return projections.saint_m_et_b(mean_coords);
    case "SAINT-PIERRE-ET-MIQUELON":
      return projections.saint_p_et_m(mean_coords);
    case "POLYNESIE-FRANCAISE":
      return projections.polynesie(mean_coords);
    case "WALLIS-ET-FUTUNA":
      return projections.wallis_et_futuna(mean_coords);
    case "NOUVELLE-CALEDONIE":
      return projections.kanaky(mean_coords);
    case "ETRANGER":
      return projections.monde(mean_coords);
    case "PARIS":
    case "SEINE-SAINT-DENIS":
    case "HAUTS-DE-SEINE":
    case "VAL-DE-MARNE":
      return projections.ile_de_france(mean_coords);
    default:
      return projections.metropole(mean_coords);
  }
}


/* -- Circles stuff -- */

function get_circo_parti(id) {
  for (let coalition of results[year]) {
    for (let parti of coalition.partis) {
      if (parti.seats.includes(id)) {
        return parti.name;
      }
    }
  }
  return "BUG!";
}

function get_circo_color(id) {
  for (let coalition of results[year]) {
    for (let parti of coalition.partis) {
      if (parti.seats.includes(id)) {
        return parti.color;
      }
    }
  }
  return "grey";
}

function get_coa_legend_x(name) {
  const nb_coa = results[year].length;
  const width_coa = width / nb_coa;

  let coa_number = 0;
  for (let coalition of results[year]) {
    if (coalition.name === name) {
      break;
    }
    ++coa_number;
  }

  if (coa_number === nb_coa) {
    console.log(`Bug: Coa not found for ${name}`);
    return NaN;
  }
  return width_coa * (coa_number + .5);

}

function get_circle_position(id) {
  const nb_coa = results[year].length;
  const nb_column_per_coa = 8; // TODO: make it dynamic
  const width_coa = width / nb_coa;

  let coa_number = 0;
  let number_in_coa;
  outer_loop: for (let coalition of results[year]) {
    number_in_coa = 0;
    for (let parti of coalition.partis) {
      if (parti.seats.includes(id)) {
        number_in_coa += parti.seats.indexOf(id);
        break outer_loop;
      }
      else {
        number_in_coa += parti.seats.length;
      }
    }
    ++coa_number;
  }

  if (coa_number === nb_coa) {
    console.log(`Bug: Coa not found for ${id}`);
    return [NaN, NaN];
  }

  let nb_seats_coa = results[year][coa_number].partis.reduce(function(sum, e){
    return sum + e.seats.length; }, 0);

  let nb_rows_coa = Math.floor(nb_seats_coa / nb_column_per_coa); // +1
  let is_topmost_row = number_in_coa >= (nb_rows_coa * nb_column_per_coa);
  let nb_seats_in_row = is_topmost_row
    ? nb_seats_coa - (nb_rows_coa * nb_column_per_coa)
    : nb_column_per_coa;

  let place_in_row = number_in_coa % nb_column_per_coa;
  let x_in_coa = (place_in_row - ((nb_seats_in_row - 1) / 2)) * circle_r * 3;

  let coa_x = width_coa * (coa_number + .5);
  let x = coa_x + x_in_coa ;
  let y =
    height
    - legend_to_bar_margin
    - (Math.floor(number_in_coa /  nb_column_per_coa) * circle_r * 3);
  return [x, y];
}


/* -- Bars stuff -- */

let bar_x = d3.scaleLinear().range([0, width]);
let bar_y = d3.scaleLinear()
  .range([height + circle_r + 1 - legend_to_bar_margin, 0]);


/* -- Load data -- */

let geojson_loaded = d3.json("france-circonscriptions-legislatives-2012.json")
  .then(function(json){ geojson = json; });
let results_json_loaded = d3.json("results.json")
  .then(function(json){ results = json; });
let noms_propres_loaded = d3.json("noms-propres.json")
  .then(function(json){ noms_propres = json; })

function check_data() {
  // results.json is handwritten, need to be checked
  for (let year in results) {
    let circos = [];
    results[year].forEach(function(coalition) {
      coalition.partis.forEach(function(parti) {
        parti.seats.forEach(function(seat) {
          if(seat.length !== 5) {
            console.log(`[WARNING] ${seat} is a weird circo ID`);
          }
          if(circos.includes(seat)) {
            console.log(
              `[WARNING] In ${year}, ${seat} is defined multiple times`);
          }
          else {
            circos.push(seat);
          }
        });
      });
    });
    if(circos.length !== 577) {
      console.log(`[WARNING] ${circos.length} seats defined in ${year}`);
    }
  }
}

function rewind_etranger() {
  // World was creating through a software that build it backward, fixin it here
  geojson.features = geojson.features.map(function(f) {
    if(f.properties.nom_dpt === "ETRANGER") {
      return turf.rewind(f, {reverse: true});
    }
    else {
      return f;
    }
  })
}

/*
 * This modifies the `results` variable.
 * For each party, add a number representing the sum of votes of previous
 * parties in its coalition. Also add a number per year: the biggest number of
 * votes in a coalition.
 */
function sum_previous_parties_votes_in_coa() {
  for (let year in results) {
    let max_votes_of_a_coa = 0;
    for (let coalition of results[year]) {
      let votes_sum = 0;
      for (let party of coalition.partis) {
        party.sum_previous_votes_in_coa = votes_sum;
        if (party.color !== "#0000") {  // Ignore invisibles
          votes_sum += party.votes;
        }
      }
      max_votes_of_a_coa = Math.max(max_votes_of_a_coa, votes_sum);
    }
    results[year]["max_votes_of_a_coa"] = max_votes_of_a_coa
  }
}

function set_bars_scales_domains() {
  bar_x.domain([0, results[year].length]);
  bar_y.domain([0, results[year].max_votes_of_a_coa]);
}

Promise.all([geojson_loaded, results_json_loaded, noms_propres_loaded])
  .then(function(){
    check_data();
    rewind_etranger();
    sum_previous_parties_votes_in_coa();
    set_bars_scales_domains();
    create_map();
    update_hidden_bars();
    d3.select("select#viz-type").attr("visibility", "visible");
    d3.select("select#annee")   .attr("visibility", "visible");
  });


/* -- Data driven stuff -- */

function display_tooltip(event) {
  let pointer = d3.pointer(event, svg.node());
  tooltip
    .style("right", "")  // Unstick what below IF might have done before
    .style("left", pointer[0]                      + 'px')
    .style("top",  pointer[1] + header_height - 25 + 'px')
    .style("display", "inline");
  if(parseFloat(tooltip.style("right")) <= 0) {
    // Tooltip further right than the window, just stick it to the right
    tooltip
      .style("left", "auto")
      .style("right", "0px");
  }
}

function display_tooltip_circo(event, d) {
  display_tooltip(event);
  tooltip_circo .style("display", "");
  tooltip_1st_br.style("display", "");
  tooltip_votes .style("display", "none");

  let nom_departement = noms_propres[d.properties.nom_dpt]
  tooltip_circo.text( nom_departement + " " + d.properties.num_circ);
  tooltip_parti.text(get_circo_parti(d.properties.ID));
}

function dislay_tooltip_votes(event, d) {
  display_tooltip(event);
  tooltip_circo .style("display", "none");
  tooltip_1st_br.style("display", "none");
  tooltip_votes .style("display", "");

  tooltip_parti.text(d.name);
  tooltip_votes.text(`${d.votes.toLocaleString()} voix`);
}

function create_map() {
  svg.selectAll("path.circo")
    .data(geojson.features)
    .enter().append("path")
      .attr("id", function(d){ return "path-" + d.properties.ID; })
      .attr("class", "circo")
      .attr("d", path)
      .attr("fill", function(d){ return get_circo_color(d.properties.ID); })
      .attr("opacity", 1)
      .on("mousemove", display_tooltip_circo)
      .on("mouseout", function(){ tooltip.style ("display", "none"); })
  ;
}

function place_on_map_then_shrink_circles() {
  svg.selectAll("circle.circo")
    .on("mousemove", null)
    .on("mouseout", null)
    .transition().duration(durations_time)
      .attr("cx", function(d){ return get_feature_center(d)[0]; })
      .attr("cy", function(d){ return get_feature_center(d)[1]; })
    .transition().duration(durations_time)
      .attr("r", 0);
  svg.selectAll("text.coa-legend")
    .transition().duration(durations_time)
      .attr("opacity", 0);
}

function show_map(delay = 1) {
  svg.selectAll("path.circo")
    .on("mousemove", display_tooltip_circo)
    .on("mouseout", function(){ tooltip.style ("display", "none"); })
    .transition().delay(delay * durations_time).duration(durations_time)
      .attr("opacity", 1);
}

function write_coa_legend() {
  svg.selectAll("text.coa-legend")
    .data(results[year])
    .join(
      function(enter) {
        enter.append("text")
          .attr("x", function(d){ return get_coa_legend_x(d.name); })
          .attr("y", height + margin.bottom)
          .transition().duration(durations_time)
            .attr("class", "coa-legend")
            .attr("y", height)
            .attr("text-anchor", "middle")
            .attr("dominant-baseline", "auto")
            .attr("opacity", 1)
            .text(function(d){ return d.name; })
      },
      function(update) {
        update
          .transition().duration(durations_time)
          .attr("x", function(d){ return get_coa_legend_x(d.name); })
          .attr("opacity", 1)
          .text(function(d){ return d.name; })
      },
      function(exit) {
        exit
          .transition().duration(durations_time)
          .attr("y", height + margin.bottom)
          .attr("opacity", 0)
          .remove();
      }
    )
}

function create_circles(current_display) {
  let cx, cy;
  if (current_display === "map") {
    cx = function(d){ return get_feature_center(d)[0]; };
    cy = function(d){ return get_feature_center(d)[1]; };
  }
  else if (current_display === "bars") {
    cx = function(d){ return get_circle_position(d.properties.ID)[0]; };
    cy = height - legend_to_bar_margin;
  }
  svg.selectAll("circle.circo")
    .data(geojson.features)
    .enter().append("circle")
      .attr("id", function(d){ return "circle-" + d.properties.ID; })
      .attr("class", "circo")
      .attr("r", 0)
      .attr("cx", cx)
      .attr("cy", cy)
      .attr("fill", function(d){ return get_circo_color(d.properties.ID); });
}

function fade_map() {
  svg.selectAll("path.circo")
    .on("mousemove", null)
    .on("mouseout", null)
    .transition().duration(durations_time)
      .attr("opacity", 0);
}

function show_circles_then_bar_them(two_steps = false) {
  let circles = svg.selectAll("circle.circo")
    .on("mousemove", display_tooltip_circo)
    .on("mouseout", function(){ tooltip.style ("display", "none"); })
  if (two_steps) {
    circles = circles.transition().duration(durations_time)
      .attr("r", circle_r)
      .attr("opacity", 1);
  }
  circles.transition().duration(durations_time)
    .attr("r", circle_r)
    .attr("opacity", 1)
    .attr("fill", function(d){ return get_circo_color(d.properties.ID); })
    .attr("cx", function(d){
      return get_circle_position(d.properties.ID)[0]; })
    .attr("cy", function(d){
      return get_circle_position(d.properties.ID)[1]; });
}

function update_circles_color() {
  svg.selectAll("circle.circo")
    .transition().duration(durations_time)
      .attr("fill", function(d){ return get_circo_color(d.properties.ID); });
}

function lower_circles() {
  d3.selectAll("circle.circo")
    .on("mousemove", null )
    .on("mouseout", null)
    .transition().duration(durations_time)
      .attr("cx", function(d){
        return get_circle_position(d.properties.ID)[0]; })
      .attr("cy", height - legend_to_bar_margin)
      .attr("r", 0)
      .attr("opacity", 0);
}

function update_bars() {
  // Create a g per coalition
  svg.selectAll("g.bars")
    .data(results[year])
    .join(
      function(enter) {
        enter.append("g")
          .attr("class", "bars")
          .attr("id", function(_, i){ return `bar-coa-${i}`; })
          .attr("transform", function (_, i) {
            return "translate("  + bar_x(i + .125) + ", 0)";
          });
      },
      function(update) {
        update.transition().duration(durations_time)
          .attr("transform", function (_, i) {
            return "translate("  + bar_x(i + .125) + ", 0)";
          });
      },
      function(exit) {
        exit.selectAll("rect")
          .transition().duration(durations_time)
          .attr("y", bar_y(0))
          .attr("height", 0)
          .style("opacity", 0)
          .remove();
      }
    );

  // In each g create a rect per party
  for(let i in results[year]) {
    let coalition = results[year][i];
    d3.select(`g#bar-coa-${i}`)
      .selectAll("rect")
      .data(coalition.partis)
      .join(
        function(enter) {
          enter.append("rect")
            .attr("y", bar_y(0))
            .attr("height", 0)
            .attr("width", bar_x(.75))
            .attr("fill", function(d){ return d.color; })
            .on("mousemove", dislay_tooltip_votes)
            .on("mouseout", function(){ tooltip.style ("display", "none"); })
            .transition().duration(durations_time)
              .attr("height", function(d){
                let top = bar_y(d.sum_previous_votes_in_coa + d.votes);
                let bottom = bar_y(d.sum_previous_votes_in_coa);
                return bottom - top; })
              .attr("y", function(d){
                return bar_y(d.sum_previous_votes_in_coa + d.votes); });
        },
        function(update) {
          update
            .on("mousemove", dislay_tooltip_votes)
            .on("mouseout", function(){ tooltip.style ("display", "none"); })
            .transition().duration(durations_time)
              .attr("width", bar_x(.75))
              .attr("fill", function(d){ return d.color; })
              .attr("height", function(d){
                let top = bar_y(d.sum_previous_votes_in_coa + d.votes);
                let bottom = bar_y(d.sum_previous_votes_in_coa);
                return bottom - top; })
              .attr("y", function(d){
                return bar_y(d.sum_previous_votes_in_coa + d.votes); });
        },
        function(exit) {
          exit.transition().duration(durations_time)
            .attr("width", bar_x(.75))
            .attr("y", bar_y(0))
            .attr("height", 0)
            .style("opacity", 0)  // I need .style(), but have .attr() elsehwere
            .remove();
        }
      );
  }
}

function update_hidden_bars() {
  // Create a g per coalition
  svg.selectAll("g.bars")
    .data(results[year])
    .join(
      function(enter) {
        enter.append("g")
          .attr("class", "bars")
          .attr("id", function(_, i){ return `bar-coa-${i}`; })
          .attr("transform", function (_, i) {
            return "translate("  + bar_x(i + .125) + ", 0)";
          });
      },
      function(update) {
        update.transition().duration(durations_time)
          .attr("transform", function (_, i) {
            return "translate("  + bar_x(i + .125) + ", 0)";
          });
      },
      function(exit) {
        exit.remove();
      }
    );

  // In each g create a rect per party
  for(let i in results[year]) {
    let coalition = results[year][i];
    d3.select(`g#bar-coa-${i}`)
      .selectAll("rect")
      .data(coalition.partis)
      .join(
        function(enter) {
          enter.append("rect")
            .attr("y", bar_y(0))
            .attr("height", 0)
            .attr("width", bar_x(.75))
            .attr("fill", function(d){ return d.color; });
        },
        function(update) {
          update.transition().duration(durations_time)
            .attr("width", bar_x(.75))
            .attr("fill", function(d){ return d.color; });;
        },
        function(exit) {
          exit.remove();
        }
      );
  }
}

function grow_bars() {
  d3.selectAll("g.bars rect")
    .on("mousemove", dislay_tooltip_votes)
    .on("mouseout", function(){ tooltip.style ("display", "none"); })
    .transition().duration(durations_time)
      .attr("height", function(d){
        let top = bar_y(d.sum_previous_votes_in_coa + d.votes);
        let bottom = bar_y(d.sum_previous_votes_in_coa);
        return bottom - top; })  // dynamic that
      .attr("y", function(d){
        return bar_y(d.sum_previous_votes_in_coa + d.votes); });
}

function shrink_bars() {
  d3.selectAll("g.bars rect")
    .on("mousemove", null)
    .on("mouseout", null)
    .transition().duration(1000)
      .attr("y", bar_y(0))
      .attr("height", 0);
}

function change_circo_colors() {
  d3.selectAll("path.circo")
    .transition().duration(durations_time)
    .attr("fill", function(d){ return get_circo_color(d.properties.ID); });
}


/* -- Gestion des <select> -- */

let current_display = "map";
d3.select("select#viz-type").on("change", function(){
  if (this.value === "map")
  {
    place_on_map_then_shrink_circles();
    show_map(current_display === "circle" ? 1 : .5);
    shrink_bars();
  }
  else if (this.value === "circle")
  {
    write_coa_legend();
    create_circles(current_display); // If needed, that's an enter()
    fade_map();
    show_circles_then_bar_them(current_display === "map");
    shrink_bars();
  }
  else if (this.value === "bars")
  {
    write_coa_legend();
    fade_map();
    grow_bars();
    lower_circles();
  }
  current_display = this.value;
});

let year_select = d3.select("select#annee");
let year = year_select.node().value;
year_select.on("change", function(){
  year = year_select.node().value;
  change_circo_colors();
  set_bars_scales_domains();

  if(current_display === "map")
  {
    place_on_map_then_shrink_circles();
  }
  if(current_display === "circle")
  {
    show_circles_then_bar_them();
    write_coa_legend();
  }
  else
  {
    update_circles_color();
  }
  if(current_display === "bars")
  {
    update_bars();
    lower_circles();
    write_coa_legend();
  }
  else
  {
    update_hidden_bars();
  }
});
